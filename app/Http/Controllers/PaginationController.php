<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;

class PaginationController extends Controller
{
    public function users() {
//        $users = User::SimplePaginate(3);
        $users = User::paginate(3);
        return view('pagination', compact('users'));
    }
}